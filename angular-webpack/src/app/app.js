import 'bootstrap/dist/css/bootstrap.css'

import angular from 'angular';
import ngResource from 'angular-resource';

import './services';
import './components'
import './search';

const requires = [
    ngResource,
    'app.services',
    'app.components',
    'app.search'
];

let app = () => {
    return {
        template: require('./app.html'),
        controller: 'AppCtrl',
        controllerAs: 'app'
    }
};

class AppCtrl {
    constructor() {
        this.name = 'app-name';
    }
}

const MODULE_NAME = 'app';

angular.module(MODULE_NAME, requires)
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        // delete $httpProvider.defaults.headers.common['X-Requested-With'];
        // $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
        // $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        // $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = 'api.twitter.com';
        // $httpProvider.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
        // Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS
        // $httpProvider.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS';
        // Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token
        // $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, Content-Type, X-Auth-Token';
        // $httpProvider.defaults.useXDomain = true;
    }])
    .directive('app', app)
    .controller('AppCtrl', AppCtrl);

export default MODULE_NAME;