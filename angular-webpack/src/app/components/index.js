import angular from 'angular';

let componentsModule = angular.module('app.components', []);

import Search from './search';
componentsModule.directive('search', Search);

export default componentsModule;