/**
 *
 * @returns {{template: *, controller: string, controllerAs: string}}
 */
let Search = () => {
    return {
        template: require('./search.html'),
        controller: 'SearchCtrl',
        controllerAs: '$ctrl'
    }
};

export default Search;