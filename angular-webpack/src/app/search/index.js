import angular from 'angular';

let searchModule = angular.module('app.search', []);

import SearchCtrl from './search.controller';
searchModule.controller('SearchCtrl', SearchCtrl);

export default searchModule;