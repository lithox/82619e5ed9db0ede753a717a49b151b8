/**
 *
 */
class SearchCtrl {
    constructor(TwitterService) {

        this.hashtag = 'iot';
        this.tweets = [];

        this._TwitterService = TwitterService;
    }

    get() {
        console.log("get tweet");
        this._TwitterService.initialize();
        // if (this._TwitterService.isReady()) {
            this._TwitterService.search(this.hashtag).then(function(data) {
                this.tweets = data;
            });
        // }
    }
}

export default SearchCtrl;