import angular from 'angular';
import ngResource from 'angular-resource';

let servicesModule = angular.module('app.services', [ngResource]);

import TwitterService from './twitter.service';
servicesModule.service('TwitterService', TwitterService);

export default servicesModule;