class TwitterService {

    constructor($q, $http, $resource) {
        'ngInject';
        this._$q = $q;
        this._$http = $http;
        this._$resource = $resource;

        this.oauthEndPoint = false;
        this.credentials = {
            consumerKey: "49FnloBvdbCnZCnOPQrz92oCT",
            consumerSecret: "ejbmYtyrRfqsoSwcmubY6wCcfRqUIA2DXXwz6p4HmEH2AEwOMN",
            accessToken: "28329879-RgazcwUOmWHmmgVv0BqqD25vjFgb4pbfJE1OiWEhl",
            accessTokenSecret: "m327leKbm3YwrTNDw4B8pG3M9pSDyZdiaOh0HbsNkAKMY",
        };
    }

    initialize() {

        this._$http({
            url: "https://api.twitter.com/oauth2/token",
            method: "POST",
            data: {
                grant_type: "client_credentials"
            },
            headers: {
                "Authorization": "Basic " + btoa(this.credentials.consumerKey + ':' + this.credentials.consumerSecret)
            },
            body: {
                grant_type: "client_credentials"
            }
        }).then(function (response) {
            // success
            console.log(response);
            //     this._$http.defaults.headers.common['Authorization'] = "Bearer " + response.access_token
        }, function (response) {
            // failed
            console.error("Twitter could not connect", response);
        });
    }

    search(hashtag) {
        var deferred = this._$q.defer();
        var url = 'https://api.twitter.com/1.1/search/tweets.json';
        if (hashtag) {
            url += '?q=' + hashtag;
        }

        this._$http({
            url: url,
        }).then(function (response) {
            // success
            console.log(response);
            deferred.resolve(response);
        }, function (response) {
            // error
            console.error("Twitter could not retrieve tweets", response);
        });

        return deferred.promise;
    }
}

export default TwitterService;
